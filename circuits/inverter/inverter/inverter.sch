EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 66799386
P 4600 2000
F 0 "J1" H 4708 2181 50  0000 C CNN
F 1 "In B" H 4708 2090 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 4600 2000 50  0001 C CNN
F 3 "~" H 4600 2000 50  0001 C CNN
	1    4600 2000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 1 1 6679AFDD
P 5100 1500
F 0 "U1" H 5100 1817 50  0000 C CNN
F 1 "40106" H 5100 1726 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5100 1500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 5100 1500 50  0001 C CNN
	1    5100 1500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 7 1 6679DC98
P 6250 2100
F 0 "U1" H 6480 2146 50  0000 L CNN
F 1 "40106" H 6480 2055 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6250 2100 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 6250 2100 50  0001 C CNN
	7    6250 2100
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 2 1 6679E840
P 5100 2000
F 0 "U1" H 5100 2317 50  0000 C CNN
F 1 "40106" H 5100 2226 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5100 2000 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 5100 2000 50  0001 C CNN
	2    5100 2000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 3 1 667A2066
P 5100 2500
F 0 "U1" H 5100 2817 50  0000 C CNN
F 1 "40106" H 5100 2726 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5100 2500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 5100 2500 50  0001 C CNN
	3    5100 2500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 4 1 667A29A0
P 5100 3000
F 0 "U1" H 5100 3317 50  0000 C CNN
F 1 "40106" H 5100 3226 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5100 3000 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 5100 3000 50  0001 C CNN
	4    5100 3000
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 5 1 667A35F6
P 5100 3500
F 0 "U1" H 5100 3817 50  0000 C CNN
F 1 "40106" H 5100 3726 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5100 3500 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 5100 3500 50  0001 C CNN
	5    5100 3500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:40106 U1
U 6 1 667A3F0A
P 5100 4000
F 0 "U1" H 5100 4317 50  0000 C CNN
F 1 "40106" H 5100 4226 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5100 4000 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF40106B.pdf" H 5100 4000 50  0001 C CNN
	6    5100 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 667BF0E2
P 4600 1500
F 0 "J2" H 4708 1681 50  0000 C CNN
F 1 "In A" H 4708 1590 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 4600 1500 50  0001 C CNN
F 3 "~" H 4600 1500 50  0001 C CNN
	1    4600 1500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 667C5409
P 4600 2500
F 0 "J3" H 4708 2681 50  0000 C CNN
F 1 "In C" H 4708 2590 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 4600 2500 50  0001 C CNN
F 3 "~" H 4600 2500 50  0001 C CNN
	1    4600 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 667C5D9A
P 4600 3000
F 0 "J4" H 4708 3181 50  0000 C CNN
F 1 "In D" H 4708 3090 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 4600 3000 50  0001 C CNN
F 3 "~" H 4600 3000 50  0001 C CNN
	1    4600 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 667C6185
P 4600 3500
F 0 "J5" H 4708 3681 50  0000 C CNN
F 1 "In E" H 4708 3590 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 4600 3500 50  0001 C CNN
F 3 "~" H 4600 3500 50  0001 C CNN
	1    4600 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J6
U 1 1 667C6C2E
P 4600 4000
F 0 "J6" H 4708 4181 50  0000 C CNN
F 1 "In F" H 4708 4090 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 4600 4000 50  0001 C CNN
F 3 "~" H 4600 4000 50  0001 C CNN
	1    4600 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J12
U 1 1 667C7034
P 5600 4000
F 0 "J12" H 5572 3932 50  0000 R CNN
F 1 "Out F" H 5572 4023 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5600 4000 50  0001 C CNN
F 3 "~" H 5600 4000 50  0001 C CNN
	1    5600 4000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J11
U 1 1 667C75AD
P 5600 3500
F 0 "J11" H 5572 3432 50  0000 R CNN
F 1 "Out E" H 5572 3523 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5600 3500 50  0001 C CNN
F 3 "~" H 5600 3500 50  0001 C CNN
	1    5600 3500
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J10
U 1 1 667C7A59
P 5600 3000
F 0 "J10" H 5572 2932 50  0000 R CNN
F 1 "Out D" H 5572 3023 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5600 3000 50  0001 C CNN
F 3 "~" H 5600 3000 50  0001 C CNN
	1    5600 3000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J9
U 1 1 667C7EBC
P 5600 2500
F 0 "J9" H 5572 2432 50  0000 R CNN
F 1 "Out C" H 5572 2523 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5600 2500 50  0001 C CNN
F 3 "~" H 5600 2500 50  0001 C CNN
	1    5600 2500
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J8
U 1 1 667C8311
P 5600 2000
F 0 "J8" H 5572 1932 50  0000 R CNN
F 1 "Out B" H 5572 2023 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5600 2000 50  0001 C CNN
F 3 "~" H 5600 2000 50  0001 C CNN
	1    5600 2000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J7
U 1 1 667C882A
P 5600 1500
F 0 "J7" H 5572 1432 50  0000 R CNN
F 1 "Out A" H 5572 1523 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 5600 1500 50  0001 C CNN
F 3 "~" H 5600 1500 50  0001 C CNN
	1    5600 1500
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male J13
U 1 1 667C995F
P 6250 1400
F 0 "J13" V 6312 1444 50  0000 L CNN
F 1 "+" V 6403 1444 50  0000 L CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 6250 1400 50  0001 C CNN
F 3 "~" H 6250 1400 50  0001 C CNN
	1    6250 1400
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J14
U 1 1 667C9FAD
P 6250 2800
F 0 "J14" V 6404 2712 50  0000 R CNN
F 1 "-" V 6313 2712 50  0000 R CNN
F 2 "Connector_Wire:SolderWirePad_1x01_Drill1.5mm" H 6250 2800 50  0001 C CNN
F 3 "~" H 6250 2800 50  0001 C CNN
	1    6250 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C1
U 1 1 667CAA3A
P 6900 2100
F 0 "C1" H 7015 2146 50  0000 L CNN
F 1 "2.2 uF" H 7015 2055 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 6938 1950 50  0001 C CNN
F 3 "~" H 6900 2100 50  0001 C CNN
	1    6900 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2250 6900 2600
Wire Wire Line
	6900 2600 6250 2600
Connection ~ 6250 2600
Wire Wire Line
	6250 1600 6900 1600
Connection ~ 6250 1600
Wire Wire Line
	6900 1600 6900 1950
Text Label 6550 2600 0    50   ~ 0
GND
Text Label 6550 1600 0    50   ~ 0
VCC
$EndSCHEMATC
