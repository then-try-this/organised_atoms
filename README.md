# Organised Atoms

A field trip and synth building workshop for experimenting with different semiconducting mine waste crystals in Redruth.

[Funded by Flamm](https://flamm.creativekernow.org.uk/) and run with [Cornwall Neighbourhoods for Change](https://www.cn4c.org.uk/).

# Documenting everything [here on the wiki](https://gitlab.com/then-try-this/organised_atoms/-/wikis/home)
