# Cornish mineralcore audio tests

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)
    
Recordings of various simple oscillator circuits with Cornish minerals
collected from minewaste acting as point contact transistors providing
frequency modulation between a modulator and carrier oscillator.

Bursts of noise and unpredicable variations are caused by electrons
spilling over boundaries between crystal material, surface build up of
contaminants and point contact 'cats whisker'. Most of these
semiconducting minerals provide a voltage offset and thresholds with
non-linear relationships between voltage and current. Some provide
negative resistance at certain voltage ranges when reverse biased.

Recording taken directly from the electrical signal output to a zoom
h4n recorder input with no processing or eq.

